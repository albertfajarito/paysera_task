PAYSERA TASK
----
## Applicant
Albert Fajarito

## Development Stack
1. PHP7.1

## Installation
1. ````run composer install````

## Commands
### Running Calculator
```php app.php [input file path]```

e.g.

```php app.php example.txt```
