`<?php

$GLOBALS['config'] = [

    "EUCountryCodes" => [
        'AT', 'BE', 'BG', 'CY',
        'CZ', 'DE', 'DK', 'EE',
        'ES', 'FI', 'FR', 'GR',
        'HR', 'HU', 'IE', 'IT',
        'LT', 'LU', 'LV', 'MT',
        'NL', 'PO', 'PT', 'RO',
        'SE', 'SI', 'SK',
    ],


    /**
     * Configuration grouping for profit calculation.
     *
     */
    'CLI' => [

        /**
         * Configurations of different resource connectors. Driver should have a corresponding class
         *  or a mapped class (see \App\Connects traits).
         *
         */
        'connectors' => [
            'bin' => [
                'driver' => "api",
                'url' => "https://lookup.binlist.net",
            ],
            'exchange_rate' => [
                'driver' => "api",
                'url' => "https://api.exchangeratesapi.io/latest",
            ],
        ],

        /**
         * Configurations of file reader. Driver should have a corresponding class
         * suports only snake-cased names and the actual class' filename should be in studly-case.
         *
         */
        'file_reader' => [
            'driver' => 'basic_reader',
        ],
    ]
];
