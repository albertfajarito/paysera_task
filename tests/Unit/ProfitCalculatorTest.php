<?php


namespace Tests\Unit;


use App\Calculators\ProfitCalculator;
use PHPUnit\Framework\TestCase;

class ProfitCalculatorTest extends TestCase
{
    public function testCalculateProfit()
    {
        $gross = rand(1,10000);
        $calculator = new ProfitCalculator(compact('gross'));
        $this->assertEquals($gross, $calculator->calculateProfit());
    }

    public function testCalculateProfitWithoutGross()
    {
        $calculator = new ProfitCalculator();
        $this->assertEquals(0, $calculator->calculateProfit());
    }
}
