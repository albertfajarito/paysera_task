<?php


namespace Tests\Unit;


use App\Calculators\ExchangeRateConverter;
use App\Calculators\ProfitCalculator;
use PHPUnit\Framework\TestCase;

class ExchangeRateConverterTest extends TestCase
{
    public function testCalculateProfit()
    {
        $gross = rand(1,10000);
        $exchangeRate = rand(1,100) / 100;
        $calculator = new ExchangeRateConverter(
            new ProfitCalculator( compact('gross') ),
            ['exchange_rate' => $exchangeRate]
        );
        $this->assertEquals($gross / $exchangeRate, $calculator->calculateProfit());
    }

    public function testCalculateProfitWithZeroRate()
    {
        $gross = rand(1,10000);
        $exchangeRate = 0;
        $calculator = new ExchangeRateConverter(
            new ProfitCalculator( compact('gross') ),
            ['exchange_rate' => $exchangeRate]
        );
        $this->assertEquals($gross / 1, $calculator->calculateProfit());
    }

    public function testCalculateProfitWithoutRate()
    {
        $gross = rand(1,10000);
        $calculator = new ExchangeRateConverter(
            new ProfitCalculator( compact('gross') ), []
        );
        $this->assertEquals($gross / 1, $calculator->calculateProfit());
    }
}
