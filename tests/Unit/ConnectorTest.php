<?php


namespace Tests\Unit;


use App\Connectors\Api;
use App\Connectors\Connector;
use Faker\Factory;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class ConnectorTest extends WithVirtualFileOperations
{
    protected $content, $rootPath, $filename;

    protected function setUp(): void
    {
        parent::setUp();
        extract($this->createVirtualFile(['extname' => 'json']));
        $this->filename = $filename;
        $this->content = $content;
        $this->rootPath = $rootPath;
    }

    public function testApiConnector()
    {
        $connector = new Api();
        $connector->setConnectionDetails(['url' => $this->rootPath]);
        $this->sendRequest($connector);
    }

    public function testConnectorMaker()
    {
        $connector = new Connector('api', ['url' => $this->rootPath]);
        $this->sendRequest($connector);
    }

    public function testMakeNotExistingConnectorThrowsException()
    {
        $id = Factory::create()->word();
        $this->expectExceptionMessage("Class '\App\Connectors\\".ucwords($id)."' not found");
        $connector = new Connector($id, ['url' => $this->rootPath]);
    }

    public function testMakeConnectorWithoutConfig()
    {
        $connector = new Connector('api');
        $this->assertEquals('App\Connectors\Connector', get_class($connector));
    }

    protected function sendRequest($connector)
    {
        $connector->setRequestDetails(['id' => $this->filename]);
        $this->assertEquals($this->content, $connector->sendRequest());
    }
}
