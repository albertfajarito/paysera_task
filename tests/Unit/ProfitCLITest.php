<?php


namespace Tests\Unit;


use App\Calculators\ExchangeRateConverter;
use App\Calculators\RegionalRate;
use App\ProfitCLI;
use Data\Factories\ExchangeRateApiResponse;
use Data\Factories\InputFileContent;
use Faker\Factory;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class ProfitCLITest extends WithVirtualFileOperations
{
    CONST COUNT = 20;

    protected $inputFileData, $debitCardData, $exchangeRateData;

    protected $inputFile, $exchangeRateFile;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpVirtualFiles();
    }

    public function testHandle()
    {
        $GLOBALS['config']['CLI']['connectors']['bin']['url'] = vfsStream::url($this->rootDir);
        $GLOBALS['config']['CLI']['connectors']['exchange_rate']['url'] = $this->exchangeRateFile['filePath'];
        $profitCLI = new ProfitCLI(config('CLI'));
        $profitCLI->calculateProfits($this->inputFile['filePath']);
        $computedProfits = $profitCLI->getProfits();
        $exchangeRates = json_decode($this->exchangeRateData, true)['rates'];
        foreach(explode("\n", $this->inputFileData) AS $index => $data){

            extract(json_decode($data, true));
            $countryCode = json_decode($this->debitCardData[$index], true)['country']['alpha2'];
            $isEu = in_array($countryCode, config('EUCountryCodes'));
            $percentage = $isEu? RegionalRate::EU_RATE : RegionalRate::NON_EU_RATE;
            $rate = 1;
            if(array_key_exists($currency, $exchangeRates)) {
                $rate = $exchangeRates[$currency];
            }
            $this->assertEquals(
                ($amount / $rate) * $percentage,
                $computedProfits[$index],
                "(".$amount." / ".$rate.") * ".$percentage
            );
        }
    }

    protected function setUpVirtualFiles()
    {
        $this->generateFakeData();
        $this->inputFile = $this->createVirtualFile([
            'filename' => 'input',
            'extname' => 'txt',
            'content' => $this->inputFileData
        ]);
        $this->exchangeRateFile = $this->createVirtualFile([
            'filename' => 'exchangerate',
            'extname' => 'json',
            'content' => $this->exchangeRateData
        ]);
        foreach(explode("\n", $this->inputFileData) AS $index => $fileData){
            $fileData = json_decode($fileData, true);
            $this->createVirtualFile([
                'filename' => $fileData['bin'],
                'content' => $this->debitCardData[$index]
            ]);
        }
    }

    protected function generateFakeData()
    {
        foreach(['inputFileData', 'debitCardData', 'exchangeRateData'] AS $fakedata){
            $this->{$fakedata} = $this->{'generateFake'.ucwords($fakedata)}();
        }
    }

    protected function generateFakeInputFileData($count = null)
    {
        $count = $count?? self::COUNT;
        $inputFileDataFactory = new InputFileContent($count);
        return $inputFileDataFactory->generate($count);
    }

    protected function generateFakeDebitCardData($count = null)
    {
        $count = $count?? self::COUNT;
        $debitCardData = [];
        for($i = 0; $i <= $count-1; $i++){
            $debitCardData[] = json_encode([
                "country" => [
                    "alpha2" => Factory::create()->countryCode()
                ]
            ]);
        }
        return $debitCardData;
    }

    protected function generateFakeExchangeRateData()
    {
        $exchangeRateGenerator = new ExchangeRateApiResponse();
        return $exchangeRateGenerator->generate();
    }
}
