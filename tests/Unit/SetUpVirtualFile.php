<?php


namespace Tests\Unit;


use Faker\Factory;
use org\bovigo\vfs\vfsStream;

trait VirtualizedFileSystem
{
    protected function setUpVirtualFileSystem(string $rootDir = null)
    {
        $rootDir = $rootDir?? substr(getcwd(), 1,strlen(getcwd()) - 1);
        $this->rootDir = $this->rootDir?? $rootDir;
        if(!directoryExists(vfsStream::url($this->rootDir))){
            vfsStream::setup($this->rootDir);
        }
    }

    protected function createVirtualFile(array $fileinfo)
    {
        extract($fileinfo);
        $filename = implode('.', [
            $filename?? Factory::create()->word(),
            $extname?? ""
        ]);

        $content = $content?? Factory::create()->text(100);
        $rootPath = vfsStream::url($this->vfsRootDir);
        $filePath = implode("/", [$rootPath, $filename]);

        file_put_contents($filePath, $content);

        $result[] = compact('filename', 'content', 'rootPath', 'filePath');
    }
}
