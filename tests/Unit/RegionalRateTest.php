<?php


namespace Tests\Unit;


use App\Calculators\ExchangeRateConverter;
use App\Calculators\ProfitCalculator;
use App\Calculators\RegionalRate;
use PHPUnit\Framework\TestCase;

class RegionalRateTest extends TestCase
{
    public function testCalculateProfitIsEu()
    {
        $gross = rand(1,10000);
        $isEu = true;
        $calculator = new RegionalRate(
            new ProfitCalculator( compact('gross') ), compact('isEu')
        );
        $this->assertEquals($gross * RegionalRate::EU_RATE, $calculator->calculateProfit());
    }

    public function testCalculateProfitIsNotEu()
    {
        $gross = rand(1,10000);
        $isEu = false;
        $calculator = new RegionalRate(
            new ProfitCalculator( compact('gross') ), compact('isEu')
        );
        $this->assertEquals($gross * RegionalRate::NON_EU_RATE, $calculator->calculateProfit());
    }
}
