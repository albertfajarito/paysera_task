<?php


namespace Tests\Unit;


use App\Readers\BasicReader;
use App\Readers\Reader;
use Faker\Factory;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class ReaderTest extends WithVirtualFileOperations
{
    protected $content;

    protected $rootPath;

    protected $filePath;

    protected $filename;


    protected function setUp(): void
    {
        parent::setUp();
        extract($this->createVirtualFile());
        $this->content = $content;
        $this->rootPath = $rootPath;
        $this->filePath = $filePath;
    }

    public function testBasicReaderRead()
    {
        $fileContent = $this->executeReader( new BasicReader() );
        $this->assertEquals($this->content, $fileContent);
    }

    public function testBasicReaderFactory()
    {
        $fileContent = $this->executeReader( new Reader('basic_reader') );
        $this->assertEquals($this->content, $fileContent);
    }

    protected function executeReader($reader)
    {
        $reader->setFilePath($this->filePath);
        return $reader->read();
    }
}
