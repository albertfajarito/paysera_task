<?php


namespace App;


use App\ClassPicker;

trait Reads
{
    public function setReader()
    {
        $config = $this->config['file_reader'];
        extract($config);
        $this->reader = new \App\Readers\Reader($driver);
    }
}
