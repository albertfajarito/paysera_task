<?php

namespace App;


use App\Calculators\ProfitCalculator;
use App\Profit;

class ProfitCLI extends Profit
{
    use Calculates, Connects, Reads;

    protected $config;
    protected $profits = [];

    public function handle($filepath)
    {
        $this->calculateProfits($filepath)
            ->render();
    }

    public function calculateProfits($filepath)
    {
        foreach($this->getFileContents($filepath) AS $content){

            if(!(!is_null($content) && is_array($content = json_decode($content, true)))){
                continue;
            }

            extract($content);
            $exchangeRate = $this->getExchangeRate($currency);
            $isEu = $this->checkIsEuCountry($bin);

            $this->profits[] = $this->calculator
                ->setConfiguration(['gross' => $amount, 'exchange_rate' => $exchangeRate, 'isEu' => $isEu])
                ->calculateProfit();
        }

        return $this;
    }

    public function render()
    {
        foreach($this->profits AS $profit){
            echo round($profit,2, PHP_ROUND_HALF_UP);
            echo PHP_EOL;
        }
    }

    public function getProfits()
    {
        return $this->profits;
    }

    protected function getFileContents($filepath)
    {
        $this->reader->setFilePath($filepath);
        return explode("\n", $this->reader->read());
    }

    protected function checkIsEuCountry($bin)
    {
        $resource = json_decode($this->connector['bin']
            ->setRequestDetails(['id' => $bin])
            ->sendRequest(),
            true
        );

        return in_array($resource['country']['alpha2'], config('EUCountryCodes'));
    }

    protected function getExchangeRate($currency)
    {
        $exchangeRates = json_decode($this->connector['exchange_rate']->sendRequest(), true);
        return array_key_exists($currency, $exchangeRates['rates'])?
            $exchangeRates['rates'][$currency] : 1;
    }
}
