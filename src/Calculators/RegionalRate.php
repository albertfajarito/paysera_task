<?php


namespace App\Calculators;


class RegionalRate extends ProfitCalculatorDecorator
{
    CONST EU_RATE = .01;
    CONST NON_EU_RATE = .02;

    protected $isEu;

    public function calculateProfit()
    {
        if(!is_null($this->isEu)){
            $rate = $this->isEu? self::EU_RATE : self::NON_EU_RATE;
            return $this->calculator->calculateProfit() * $rate;
        }
    }

    public function setConfiguration(array $config = [])
    {
        extract($config);
        $this->isEu = $isEu ?? null;
    }
}
