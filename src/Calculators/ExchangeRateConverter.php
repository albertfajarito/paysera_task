<?php


namespace App\Calculators;


class ExchangeRateConverter extends ProfitCalculatorDecorator
{
    protected $rate = 1;

    public function calculateProfit()
    {
        return $this->calculator->calculateProfit() / $this->rate;
    }

    public function setConfiguration(array $config = [])
    {
        extract($config);
        if(isset($exchange_rate) && $exchange_rate !== 0){
            $this->rate = $exchange_rate;
        }
    }
}
