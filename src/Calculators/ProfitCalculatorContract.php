<?php


namespace App\Calculators;


interface ProfitCalculatorContract
{
    public function calculateProfit();
}
