<?php


namespace App\Calculators;


class ProfitCalculator implements ProfitCalculatorContract, ConfigurableCalculator
{
    protected $gross;

    public function __construct(Array $config = [])
    {
        $this->setConfiguration($config);
    }

    public function calculateProfit()
    {
        return $this->gross;
    }

    public function setConfiguration(array $config = [])
    {
        extract($config);
        $this->gross = $gross ?? 0;
    }
}
