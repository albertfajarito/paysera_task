<?php


namespace App\Calculators;


class RegionalProfitCalculator implements ProfitCalculatorContract, ConfigurableCalculator
{
    protected $gross, $exchangeRate, $isEu;

    public function calculateProfit()
    {
        if(is_null($this->isEu)){
            throw new \Exception('must declare if eu region as boolean value, null passed');
        }

        if($this->gross == 0){
            return 0;
        }

        $config = [
            'exchange_rate' => $this->exchangeRate,
            'gross' => $this->gross,
            'isEu' => $this->isEu,
        ];

        $calculator = new ExchangeRateConverter(
            new RegionalRate(
                new ProfitCalculator($config), $config
            ),
            $config
        );

        return $calculator->calculateProfit();
    }

    public function setConfiguration(array $config = [])
    {
        extract($config);
        $this->isEu = $isEu ?? null;
        $this->exchangeRate = $exchange_rate ?? 0;
        $this->gross = $gross ?? 0;
        return $this;
    }
}
