<?php


namespace App\Calculators;


use App\Profit;

abstract class ProfitCalculatorDecorator implements ProfitCalculatorContract, ConfigurableCalculator
{
    protected $calculator;

    public function __construct(ProfitCalculatorContract $calculator, Array $config = [])
    {
        $this->calculator = $calculator;
        $this->setConfiguration($config);
    }

    abstract public function calculateProfit();

    abstract public function setConfiguration(array $config = []);
}
