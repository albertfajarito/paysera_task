<?php


namespace App\Calculators;


interface ConfigurableCalculator
{
    public function setConfiguration(Array $config);
}
