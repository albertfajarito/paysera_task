<?php

namespace App\Connectors;


class Api implements ConnectorContract
{
    protected $url, $stubId;

    public function sendRequest()
    {
        $url = implode("/", array_filter([$this->url, $this->stubId ?? ""]));

        try{
            return file_get_contents($url);
        }
        catch(\Exception $e){
            throw new \Exception($e);
        }
    }

    public function setRequestDetails(array $param)
    {
        extract($param);
        $this->stubId = $id ?? "";
    }

    public function setConnectionDetails(array $detl)
    {
        extract($detl);
        $this->url = $url ?? "";
    }
}
