<?php

namespace App\Connectors;


interface ConnectorContract
{
    public function sendRequest();

    public function setRequestDetails(Array $param);

    public function setConnectionDetails(Array $detl);
}
