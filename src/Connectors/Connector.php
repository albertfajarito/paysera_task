<?php


namespace App\Connectors;


use App\ClassPicker;

class Connector
{
    use ClassPicker;

    protected $connection;

    public function __construct($identifier, $param = [])
    {
        $this->connection = $this->getModuleClass($identifier);
        $this->setConnectionDetails($param);
    }

    public function sendRequest()
    {
        return $this->connection->sendRequest();
    }

    public function setRequestDetails(array $param)
    {
        $this->connection->setRequestDetails($param);
        return $this;
    }

    public function setConnectionDetails(array $detl)
    {
        $this->connection->setConnectionDetails($detl);
    }
}
