<?php


namespace App\Readers;


interface ReaderContract
{
    public function read();

    public function setFilePath($path);
}
