<?php


namespace App\Readers;


use App\ClassPicker;

class Reader implements ReaderContract
{
    use ClassPicker;

    protected $reader;

    public function __construct($identifier, $param = null)
    {
        $this->reader = $this->getModuleClass($identifier, $param);
    }

    public function read() : string
    {
        return $this->reader->read();
    }

    public function setFilePath($path) : void
    {
        $this->reader->setFilePath($path);
    }
}
