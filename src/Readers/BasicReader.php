<?php

namespace App\Readers;


class BasicReader implements ReaderContract
{
    protected $filepath;

    public function __construct($filepath = null)
    {
        $this->setFilePath($filepath ?? "");
    }

    public function read()
    {
        return file_get_contents($this->filepath);
    }

    public function setFilePath($path)
    {
        $this->filepath = $path;
    }
}
