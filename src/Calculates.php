<?php


namespace App;


trait Calculates
{
    public function setCalculator($calculatorType)
    {
        $className = "\\App\Calculators\\".$calculatorType."Calculator";
        $this->calculator = new $className();
    }
}
