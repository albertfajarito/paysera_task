<?php


namespace App;


use App\Calculates;
use App\Connects;
use App\Reads;

abstract class Profit
{
    CONST DEFAULT_CALCULATOR = "RegionalProfit";

    use Calculates, Connects, Reads;

    protected $calculator, $connector, $reader, $amount, $countryCode, $rate;

    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setReader();
        $this->setConnectors();
        $this->setCalculator(self::DEFAULT_CALCULATOR);
    }

    abstract public function handle($path);
}
