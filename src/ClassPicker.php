<?php


namespace App;


trait ClassPicker
{

    protected function getModuleClass($className, $param = null)
    {
        $classNS = "\\".$this->getRootNamespace()."\\".$this->toStudly($className);

        if(isset($param)) {
            if(is_array($param)){
                $obj = new $classNS(...$param);
            } else {
                $obj = new $classNS($param);
            }
        } else {
            $obj = new $classNS();
        }

        return $obj;
    }

    protected function getRootNamespace()
    {
        $reflection = new \ReflectionClass($this);
        return $reflection->getNamespaceName();;
    }

    protected function toStudly($string)
    {
        return str_replace(" ", "" ,ucwords(str_replace("_", " ", $string)));
    }
}
