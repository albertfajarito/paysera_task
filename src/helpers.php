<?php

if(!function_exists('config')){
     function config($keys){

        $config = $GLOBALS['config'];

        foreach(explode(".", $keys) AS $key){
            $config = $config[$key];
        }

        return $config;
    }
}
