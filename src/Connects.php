<?php


namespace App;


trait Connects
{
    public function setConnectors()
    {
        foreach($this->config['connectors'] AS $name => $arr){
            $config = $this->config['connectors'][$name];
            extract($config);
            $this->connector[$name] = new \App\Connectors\Connector( $driver, ['url'=>$url] );
        }
    }
}
