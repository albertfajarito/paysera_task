<?php

namespace Data\Factories;


class ExchangeRateApiResponse extends ProfitFactory
{
    public function generate()
    {
        return json_encode([
            "rates" => array_combine(parent::CURRENCY_CODES, $this->makeRates(count(parent::CURRENCY_CODES))),
            "base" => parent::DEFAULT_CURRENCY_CODE,
            "date" => date('Y-m-d', time())
        ]);
    }

    public function makeRates($count)
    {
        $rates = [];
        for($i = 1; $i <= $count; $i++){
            $rates[] = rand(1,100) / 100;
        }
        return $rates;
    }
}
