<?php

namespace Data\Factories;


class InputFileContent extends ProfitFactory
{
    CONST DATA_FIELDS = ['bin', 'amount', 'currency'];

    CONST SEPERATOR = "\n";

    CONST CHANCE_AMOUNT_IS_ZERO = .3;

    CONST CHANCE_CURRENCY_IS_DEFAULT = .3;

    public function generate($count)
    {
        $data = [];

        for($i = 0; $i < $count; $i++){
            $data[] = json_encode($this->makeData());
        }

        return implode(self::SEPERATOR, $data);
    }

    public function makeData()
    {
        $data = [];
        foreach(self::DATA_FIELDS AS $field)
        {
            $methodName = "make".ucwords(strtolower($field))."Data";
            if(method_exists($this, $methodName)){
                $data[$field] = $this->{$methodName}();
            } else {
                $data[$field] = "";
            }
        }
        return $data;
    }

    public function makeBinData()
    {
        return rand(1000,9999);
    }

    public function makeAmountData()
    {
        return rand(1,10) <= round(10*self::CHANCE_AMOUNT_IS_ZERO, 0, PHP_ROUND_HALF_UP)?
            0 : rand(100,10000);
    }

    public function makeCurrencyData()
    {
        return rand(1,10) <= round(10*self::CHANCE_CURRENCY_IS_DEFAULT, 0 , PHP_ROUND_HALF_UP)?
            parent::DEFAULT_CURRENCY_CODE : self::CURRENCY_CODES[rand(0, count(self::CURRENCY_CODES)-1)];
    }
}
