<?php


namespace Data\Factories;


use Faker\Factory;

class ProfitFactory extends Factory
{
    CONST DEFAULT_CURRENCY_CODE = 'EUR';

    CONST CURRENCY_CODES = [
        "CAD", "HKD", "ISK", "PHP", "DKK",
        "HUF", "CZK", "AUD", "RON", "SEK",
        "IDR", "INR", "BRL", "RUB", "HRK",
        "JPY", "THB", "CHF", "SGD", "PLN",
        "BGN", "TRY", "CNY", "NOK", "NZD",
        "ZAR", "USD", "MXN", "ILS", "GBP",
        "KRW", "MYR",
    ];
}
