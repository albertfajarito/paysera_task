<?php

require_once realpath('vendor/autoload.php');


$calculator = new \App\ProfitCLI(config('CLI'));
$calculator->handle($argv[1]);
